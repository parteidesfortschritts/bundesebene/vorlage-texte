import re
from functools import reduce
import os.path
import argparse

"""
initial version from: http://connor-johnson.com/2014/12/13/writing-a-markdown-to-latex-converter-in-python/
"""


def heading(line, nhashes):
    """
    Returns True if line is a heading, otherwise False
    """
    # if there are an even number of hashes
    if nhashes > 0 and re.match("^#{" + str(nhashes) + "}", line):
        return True
    return False


def parse_heading(line, nhashes):
    """
    Determine the level of the heading

    Return '\section', '\subsection', or '\subsubsection'
    """
    # if not a heading, return line
    if not heading(line, nhashes):
        return line
    # determine heading level
    level = nhashes
    # grab the heading content
    content = line.split("#" * level)
    content = [i.strip() for i in content if i != ""]
    content = [i.replace("\\\\", "") for i in content]
    text = content[0].replace (" ", "-")
    label = "{link:" + content[0] + "}"
    content = "{" + sanitizeTexString(content[0])  + "}"
    # sort out the heading levels
    if level == 1:
        out = r"\section{0} " + r"\label{1}"
    elif level == 2:
        out = r"\subsection{0} " + r"\label{1}"
    elif level == 3:
        out = r"\subsubsection{0} "+ r"\label{1}"
    else:
        return line
    # format the output
    return out.format(content, label) #, label


def look_for_lists(x, marker, typelist):
    """
    Backend for ordered and unordered lists functions.
    """
    maxlevel = 0
    # search for tabs
    for i, line in enumerate(x):
        currentLevel = line.count('\t')
        if(currentLevel > maxlevel):
            maxlevel = currentLevel
    maxlevel = maxlevel+1
    for level in range (0, maxlevel):
        regexQuery = "^"
        regexQueryLower = "^"
        lines = list()
        for tabs in range(0,level):
            regexQuery = regexQuery + "\t"
        regexQuery = regexQuery + marker+ "\s"

        for tabs in range(0,level+1):
            regexQueryLower = regexQueryLower + "\t"
        regexQueryLower = regexQueryLower + marker+ "\s"

       # create a list of lists or indices that
        # have some sort of indented marker
        for i, line in enumerate(x):
            if re.match(regexQuery, line):
                if (lines == []) or (i - 1 != lines[-1][-1]):
                    lines.append([i])
                else:
                    lines[-1].append(i)
            else:
                if re.match(regexQueryLower, line):
                    if(lines == []): 
                        lines.append([i])
                    else:
                        lines[-1].append(i)
            
        # if no indented markers are found, return
        if lines == []:
            return x
        # replace the markers with \item tages
        for group in lines:
            for i, line in enumerate(group):
                x[line] = re.sub(regexQuery, r"  \\item ", x[line])

        # determine where to put the \begin{} and \end{} delimiters
        idx = 0
        for i, group in enumerate(lines):
            lines[i] = [group[0] + idx, group[-1] + idx + 2]
            idx += 2
        
        lines = reduce(lambda u, v: u + v, lines)
        # insert the appropriate \begin{} and \end{} delimiters
        for i, idx in enumerate(lines):
            if i % 2 == 0:
                x.insert(idx, r"\begin{" + typelist + "}") #\arabic
            else:
                x.insert(idx, r"\end{" + typelist + "}")
    
    return x


def ordered_lists(x):
    """
    Use a regex to find numbered lists
    """
    return look_for_lists(x, "[0-9]+\.", "enumerate")


def unordered_lists(x):
    """
    Use a regex to find unordered lists
    """
    return look_for_lists(x, "[-\*\+o]", "itemize")


def absatz(x):
    if re.match(r"^.*?:\s{4}", x):
        counter = x.find(":")
        absatztext = x[counter:]
        text = x[:counter]
        x = r"\absatz{" + text + "}" + absatztext[4:]
    return x


def kommentar(x):
    counter = 0
    for i in range(len(x)):
        if re.match(r"^```", x[i]):
            if counter == 0:
                x[i] = "\comment{\n" + x[i][3:]
                counter += 1
            else:
                x[i] = "\n}"
    return x


def link(x):
    if re.match(r"/^\[([\w\s\d]+)\]\(((?:\/|https?:\/\/)[\w\d./?=#]+)\)$/", x):
        print(x)
    return x

def open_markdown(fn):
    """
    Open a file, and return a list of strings
    """
    x = open(fn, "r").read()
    x = x.replace("\n\n\n", "\\\\\\\n\n\n")
    x = x.replace("\n\n", "\\\\\n\n")
    x = x.splitlines()
    x = [i.rstrip() for i in x]
    return x


def parse_document(x):
    """
    Parse and convert a Markdown document into LaTeX

    Supports three levels of headings, ordered and unordered lists
    """
    y = list()
    # parse headings
    for line in x:
        nhashes = line.count("#")
        y.append(parse_heading(line, nhashes))
    # parse ordered lists
    y = kommentar(y)
    y = list(map(absatz, y))
    y = ordered_lists(y)
    # parse unordered lists
    y = unordered_lists(y)
    # add some simple headers and footers
    return y


def write_latex(fn, y):
    """
    Write a list of strings to a LaTeX file.
    """
    fh = open(fn, "w")
    for line in y:
        fh.write(line + "\n")
    fh.close()


def convert(md_doc, latex_doc):
    x = open_markdown(md_doc)
    y = parse_document(x)
    write_latex(latex_doc, y)

def sanitizeTexString(string, strip="%$}{^_#&~", replacement=[r"\%", r"\$", r"\}", r"\{", r"\^{}", r"\_{}", r"\#", r"\&", r"\char`\~"]):
    for i, char in enumerate(strip):
        string = string.replace(char, replacement[i])
    return string


parser = argparse.ArgumentParser(description="Convert Markdown to LaTeX")
parser.add_argument("input", help="Path to input Markdown file")
parser.add_argument("output", help="Path to output LaTeX file")

# Parse arguments
args = parser.parse_args()
convert(os.path.abspath(args.input), args.output)