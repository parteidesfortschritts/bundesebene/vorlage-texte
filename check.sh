#/bin/sh
latexmk -pdf -lualatex -quiet main.tex  ; REPLY=$?
if [ ! -f main.pdf ]
then 
    echo "PDF not found"
    exit 1
else
    mv main.pdf satzung.pdf
    exit 0
fi
