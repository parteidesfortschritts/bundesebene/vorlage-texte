# satzung

[(nicht mehr) aktuelle Satzung als pdf](/../-/jobs/artifacts/master/raw/satzung.pdf?job=latex2pdf)


# Wichtige Hinweise für die MD Files
* Keine Schachtelung von Listen über 5 Ebenen
* "Topic: \t" für Laufende Punkte in Gesetztestexten
* % nur mit \% schreiben


# start script
example
python md2latex/md2latex.py ./grundsatzprogramm/text.md ./grundsatzprogramm/text.tex

compile tex to pdf
latexmk -pdf -lualatex -quiet /grundsatzprogramm/main.tex 