# Name, Sitz und Tätigkeitsgebiet

Name:    Der Name der Partei lautet „Partei des Fortschritts“. Als Kurzbezeichnung verwendet sie (in dieser Schreibweise) die Buchstabenkombination PdF.
Sitz:    Die Partei hat ihren Sitz in Köln, solange nicht Größe oder Zielsetzung der Partei etwas anderes erforderlich machen. Dies kann insbesondere der Fall sein, wenn die Partei mit eigenen Abgeordneten im deutschen Bundestag vertreten ist. Die Entscheidung, ob Größe oder Zielsetzung der Partei etwas anderes erforderlich machen, obliegt dem Vorstand des Bundesverbands.
Tätigkeitsgebiet:    Die Partei des Fortschritts ist im gesamten Bundesgebiet, sowie im Rahmen der europäischen Union auch europaweit tätig. Die Partei des Fortschritts hat den Anspruch, potenziell alle Bürger durch ihre Tätigkeit zu vertreten.
Zweck:    Zweck der Partei ist die Mitgestaltung eines demokratischen Staats- und Gemeinwesens: Die Partei wirkt an der Gestaltung eines demokratischen Staats und Gemeinwesens mit, das allen Menschen ein selbstbestimmtes Leben ermöglichen soll. Sie strebt dabei insbesondere an, den Planeten Erde als Biosphäre des Menschen dauerhaft zu erhalten und die Lebenssituation aller in Deutschland lebenden Menschen – vor allem mit Blick auf die Zukunft – dauerhaft und nachhaltig zu verbessern. Außerdem das gesellschaftliche Zusammenleben nach rechtsstaatlichen Prinzipien möglichst so zu gestalten, dass jeder Mensch ein Recht auf sichere Existenz und gesellschaftliche Teilhabe hat und das jeder Mensch über Zugang zu allen Informationen verfügt, die für selbstbestimmt und frei getroffene Entscheidungen nötig sind.

# Aufnahme und Austritt der Mitglieder

Antrag auf Mitgliedschaft:    Der Beitritt in die Partei des Fortschritts ist durch mündliche Erklärung gegenüber einem Organ oder Organwalter der Partei oder durch schriftlichen Antrag möglich. Es besteht kein Formerfordernis. Das Organ oder der Organwalter, welchen den mündlichen oder schriftlichen Antrag entgegengenommen hat, legt diesen zur Prüfung dem Vorstand des zuständigen Gebietsverbands zur Bestätigung vor. Erhebt der entsprechende Vorstand innerhalb von 7 Tagen keine Einwände gegen die Mitgliedschaft, gilt diese als bestätigt. Die Mitgliedschaft wird direkt bei der Partei erworben.

Mitgliedschaft:    Eine Mitgliedschaft steht jeder natürlichen Person offen, die ihren Hauptwohnsitz in der Bundesrepublik Deutschland hat oder einen Eintrag in ein Wahlregister der Bundesrepublik Deutschland nachweisen kann oder die deutsche oder die europäische Unionsbürgerschaft besitzt. Mit dem Beitritt erklärt sich das Mitglied mit der Verarbeitung personenbezogener Daten durch die Partei einverstanden. Mit dem Beitritt erklärt das Mitglied weiterhin sein Einverständnis zu Satzung und Programm der Partei. Ein Mindestalter besteht nicht. Personen, die das 18. Lebensjahr noch nicht vollenden haben, bedürfen zum Beitritt der schriftlichen Einverständniserklärung einer erziehungsberechtigen Person.

Austritt:    Der Austritt aus der Partei des Fortschritts ist durch schriftliche oder mündliche Erklärung gegenüber der Partei möglich.

Pflichten aus besonderer Verantwortung:    Eine Mitgliedschaft in der Partei des Fortschritts ist unvereinbar mit der Mitgliedschaft in einer Partei oder Organisation, die dem Zweck der Partei des Fortschritts nach Art. 1 (4) der Satzung oder der freiheitlich-demokratischen Grundordnung der Bundesrepublik Deutschland oder dem Gedanken der Völkerverständigung zu wieder läuft. Hat eine solche Mitgliedschaft in der Vergangenheit bestanden, hat der Antragsteller auf Mitgliedschaft dies der für den Beitritt zuständigen Parteistelle anzugeben. Die zuständige Parteistelle prüft, ob gewichtige Gründe für die Annahme vorliegen, dass der Antragsteller sich ideologisch und politisch von der entsprechenden Partei oder Organisation abgewandt hat. Sie legt das Ergebnis ihrer Prüfung dem für den Gebietsverband zuständigen Parteiparlament vor. Die Prüfung welche Parteien oder Organisationen unvereinbar im Sinne dieses Absatzes sind, obliegt dem Bundesparteiparlament. Wird eine bestehende oder vergangene Mitgliedschaft in einer mit der Partei des Fortschritts unvereinbaren Organisation oder Partei beim Beitritt durch das Mitglied verschwiegen, führt dies zum Ausschluss aus der Partei, sofern dem nicht gewichtige Gründe entgegenstehen. Über das Vorliegen gewichtiger Gründe im Sinne dieses Absatzes entscheidet das Bundesparteiparlament. Es gilt ferner das Verfahren des Art. 4 (4) dieser Satzung.

Beginn der Mitgliedschaft:    Die vorläufige Mitgliedschaft beginnt mit der Entgegennahme der mündlichen Beitrittserklärung oder des schriftlichen Mitgliedsantrags. Die Vollmitgliedschaft wird bei Bestätigung der Mitgliedschaft durch den Vorstand des zuständigen Gebietsverbands oder des Ablaufes der Bestätigungsfrist begründet. Voraussetzung für die Erlangung der Vollmitgliedschaft ist, dass sich der Antragsteller mittels eines amtlichen Personalausweises, ein Reisepass, gegebenenfalls einer Aufenthaltsgenehmigung, eines amtlichen Nachweises der Unionsbürgerschaft, eines amtlichen Ausweises eines Mitgliedstaats der europäischen Union oder seiner Geburtsurkunde unter Angabe seines vollen Namens amtlichen Wohnsitzes, seiner Telefonnummer und Emailadresse bei der zuständigen Parteistelle akkreditiert hat.

Anzeigepflichten:    Jedes Mitglied ist verpflichtet, der Partei Änderungen des Wohnsitzes, der Eintragung in ein Wählerregister, der Telefonnummer, der Emailadresse und des Namens mitzuteilen. Ferner hat es die Übernahme öffentlicher Ämter oder die Arbeit in wirtschaftlichen Interessenverbänden der Partei anzuzeigen.

Ende der Mitgliedschaft:    Die Mitgliedschaft endet bei Tod des Mitglieds, bei Erklärung des Austritts aus der Partei gegenüber einem Organ oder Organwalter der Partei oder bei Ausschluss des Mitglieds wegen schwerwiegender Verfehlungen gegen Satzung, Programm oder Zielsetzung der Partei des Fortschritts.

# Rechte und Pflichten der Mitglieder

Mitgliedsbeitrag:    Solange dies für die Finanzierung der Parteitätigkeit nicht notwendig ist, wird kein Mitgliedsbeitrag erhoben. Die Entscheidung ab wann ein für alle Mitglieder verbindlicher Mitgliedsbeitrag für die Finanzierung der Parteitag notwendig ist trifft das Bundesparteiparlament auf Antrag des Parteisprechers oder des Bundesvorstands. Sobald ein Mitgliedsbeitrag erhoben werden soll, ist dieser in verhältnismäßiger Weise zu staffeln anhand von: Monatsnettoeinkommen, familiärer und sozialer Gesamtsituation und Berufsstands des einzelnen Mitglieds. Ein Mitglied kann gegen die Einstufung in eine bestimmte Beitragsgruppe Beschwerde beim Vorstand des zuständigen Gebietsverbandes erheben. Dieser prüft die Beschwere in Zusammenarbeit mit dem Schatzmeister des Bundesverbandes. Liegen gewichtige Gründe vor, welche die Beschwerde untermauern, hat der zuständige Vorstand das Mitglied in die entsprechend angemessene Beitragsgruppe einzuordnen.

Mitgliedsausweis:    Jedes Mitglied hat, sobald die finanzielle Aufstellung der Partei dies zulässt, Anspruch auf die Ausstellung eines schriftlichen Mitgliedsausweises. Eine ausreichende finanzielle Ausstattung kann jedenfalls dann angenommen werden, wenn die Kosten für die Ausstellung eines schriftlichen Mitgliedsausweises an alle Mitglieder 1% des Parteivermögens nicht überschreiten. Die Entscheidung darüber wann die finanzielle Aufstellung der Partei ein entsprechendes Verfahren zulässt, trifft der Schatzmeister des Bundesverbands. Er hat die Gründe seiner Entscheidung vor dem Vorstand des Bundesverbandes darzulegen.

Petitionsrecht:    Jedes Mitglied hat ein Petitionsrecht gegenüber den Organen der Partei.

Recht zur Meinungs- und Willensbildung:    Jedes Mitglied hat das Recht, sich in eines der Organe der Partei wählen zu lassen, oder sich auf offen stehende Positionen zu bewerben.

Recht am geistigen Eigentum der Partei:    Jedes Mitglied hat das Recht, das Logo, offizielle Schriftzüge oder Slogans der Partei zu verwenden, wenn es offizielle Positionen oder Anliegen der Partei verbreitet.

Pflichten zur Verbreitung von Informationen:    Mitglieder verpflichten sich, die Tätigkeit der Partei durch das Verbreiten ihrer Botschaften zu unterstützen. Dies soll insbesondere  im Rahmen der sozialen Netzwerke, sowie dem persönlichen Bekanntenkreis geschehen.
Verbot bestimmter Ideologien und Verbot gruppenbezogener Menschenfeindlichkeit:  Mitglieder verpflichten sich, den Zielen und Anliegen der Partei nicht entgegen zu wirken und keine öffentlichen Aussagen im Namen der Partei zu tätigen, die den Grundsätzen ihres Programms entgegen stehen. Es ist Mitgliedern untersagt Ansichten oder Ideologien zu verbreiten, die dem in Art. 1 (4) der Satzung der Partei des Fortschritts definiertem Zweck der Partei oder den Grundsätzen der verfassungsmäßigen Ordnung, insbesondere der Grundrechte, der Bundesrepublik Deutschland oder den Menschenrechten oder dem Gedanken der Völkerverständigung entgegenwirken. Es ist Mitgliedern außerdem untersagt Handlungen vorzunehmen, welche solche Ansichten oder Ideologien fördern.

# Zulässige Ordnungsmaßnahmen gegen Mitglieder und Ausschluss

Erlass von Ordnungsmaßnahmen:    Die Partei kann Ordnungsmaßnahmen gegen Mitglieder erlassen, die den Zielen oder Grundsätzen der Partei entgegen wirken.

Mögliche Ordnungsmaßnahmen:    Diese beinhalten, sind aber je nach Situation nicht beschränkt auf:
1. Schriftliche Rügen.
2. Suspendierung der Mitgliedschaft in oder Ausschluss aus Parteiorganen.
3. Zeitlich begrenzte oder dauerhafte Suspendierung des Rechts Partei- und Versammlungsämter zu bekleiden.

Zuständiges Organ der Ordnungsmaßnahmen:    Die Ordnungsmaßnahmen werden durch den Vorstand angeordnet. Dieser hat erlassene Ordnungsmaßnahmen dem Bundesparteiparlament bei dessen zeitlich nächster Sitzung anzuzeigen. Bis zum ersten Zusammentreten des Bundesparteiparlaments ist die Ordnungsmaßnahme dem zeitlich nächsten Parteitag anzuzeigen.

Aberkennung der Mitgliedsrechte:    Handelt ein Mitglied dauerhaft den Zielen oder Grundsätzen der Partei und fügt ihre damit erheblichen Schaden zu oder handelt es gegen die Bindung an die freiheitlich demokratische Grundordnung der Bundesrepublik Deutschland, kann es ausgeschlossen werden. Der Ausschluss erfolgt nach Prüfung des Sachverhalts durch Entscheidung des nach der Schiedsgerichtsordnung (SchGO) der PdF zuständigen Schiedsgerichts. Gegen die Entscheidung ist eine Berufung beim Bundesschiedsgericht möglich. In dringenden und schwerwiegenden Fällen, die sofortiges Eingreifen erfordern, kann der Vorstand der Partei oder eines Gebietsverbands ein Mitglied von der Ausübung seiner Rechte bis zur Entscheidung des Schiedsgerichts ausschließen. Ein dringender und schwerwiegender Grund liegt insbesondere bei satzungswidrigem oder verfassungsfeindlichem Verhalten des Mitglieds vor.

Klagemöglichkeit gegen Ordnungsmaßnahme:    Gegen diese Maßnahmen steht dem Mitglied die Anrufung eines Schiedsgerichts offen.

# Zulässige Ordnungsmaßnahmen gegen Gebietsverbände

Wirkungsbereich:    Die Partei kann Ordnungsmaßnahmen gegen Gebietsverbände erlassen, die den Zielen oder Grundsätzen der Partei entgegen wirken oder sich in ihrem politischen Wirken gegen die freiheitlich demokratische Grundordnung der Bundesrepublik stellen oder ein entsprechendes Verhalten ihrer Mitglieder tolerieren. Sie kann auch Ordnungsmaßnahmen erlassen, wenn ein Gebietsverband dem politischen Wirken der Bundespartei entgegen wirkt oder das Gebot der Gesamtparteifreundlichkeit nicht beachtet.

Umfang:    Diese Maßnahmen beinhalten, sind aber je nach Situation nicht beschränkt auf:
1. Schriftliche Rügen.
2. Einbestellung des Vorstands oder sonstiger Führungsorgane des Gebietsverbands.
3. Abberufung des Vorstands oder sonstiger Führungsorgane des Gebietsverbands und Einsetzung einer provisorischen Verbandsführung. Diese ist auf einen Monat mandatiert und hat dafür zu sorgen, dass der Gebietsverband aus seiner Mitte eine neue Führung wählt. Suspendierte Führungsmitglieder des entsprechenden Gebietsverbands sind für ein Jahr von ihren bisherigen Positionen ausgeschlossen. Diese Abberufung erfolgt nach Prüfung des Sachverhalts durch den Vorsitzenden auf Beschluss des Vorstands.
4. Auflösung des Gebietsverbands. Die Auflösung eines Gebietsverbands ist nur zulässig, wenn die in (a)-(c) beschriebenen oder sonstige Maßnahmen die Störung durch den Gebietsverband nicht beseitigt haben.

Zulässigkeit:    Die Maßnahmen sind zulässig, wenn:
1. der Gebietsverband den Zielen, der Satzung oder den Grundsätzen der Partei über längere Dauer und beharrlich entgegenwirkt oder sich in seinem politischen Wirken gegen die freiheitlich demokratische Grundordnung der Bundesrepublik Deutschland stellt.
2. der Gebietsverband ein solches Verhalten seiner Mitglieder toleriert und dem nicht effektiv entgegen wirkt.
3. oberste Organe des Gebietsverbands Gelder der Partei veruntreut haben und der Gebietsverband seine organschaftliche und finanzielle Integrität glaubhaft aus eigener Kraft wiederherstellen kann.
4. der Gebietsverband die besondere Stellung und nach der Satzung zugewiesenen Kompetenzen der Parteiparlamente innerhalb seines Organisationsbereichs nicht respektiert und diese so in ihrer Arbeit behindert.

Vorgehensweise:    Ordnungsmaßnahmen werden durch den Vorstand der Partei getroffen. Dieser hat für die Maßnahmen auf dem nächsten Bundesparteitag die Bestätigung einzuholen. Gegen die Ordnungsmaßnahme steht dem Gebietsverband die Anrufung des nach der SchGO zuständigen Landesschiedsgerichts offen. Gegen die entsprechende Entscheidung des Landesschiedsgerichts steht dem Gebietsverband die Berufung vor dem Bundesschiedsgericht offen.

# Allgemeine Gliederung der Partei

Organe:    Die Partei gliedert sich in einen Bundesverband, den Bundesländern entsprechende Landesverbände und den Wahlkreisen entsprechende Kreisverbände.
Zuständigkeit des Bundesvorsstandes:    Der Bundesverband vertritt die Partei bundes- und europaweit. Er besitzt Richtlinienkompetenz im Hinblick auf die ideelle, politische und programmatische Ausrichtung der Partei.

Landesverbände:     Die Landesverbände vertreten die Partei in ihrem entsprechenden Bundesland. Sie gestalten selbstständig die landespolitischen Positionen der Partei, sind dabei allerdings den bundespolitischen Zielen unterworfen. Sie schaffen ihre Organe selbst.

Kreisverbände:    Die Kreisverbände vertreten die Partei in ihrem entsprechenden Wahlkreis. Sie gestalten selbständig die kommunalen Ziele der Partei, sind allerdings den landespolitischen Zielen der Partei unterworfen. Sie schaffen ihre Organe selbst.

Einberufung der Parteiparlamente:    Die Partei beruft auf allen Gliederungsebenen Parteiparlamente ein. Diese haben für ihren Verband jeweils die Funktion eines ständigen Parteitags hinsichtlich aller programmatischen – nicht partei-organisatorischen – Fragen.
1. Das Bundesparteiparlament ratifiziert die programmatischen und politischen Ziele des Bundesverbands. Es formuliert offizielle Positionen der Partei zu gesamtgesellschaftlichen oder tagesaktuellen Zielen. Seine Mitglieder werden durch den Bundesverband auf schriftliche Bewerbung berufen. Eine Bewerbung steht jedem Mitglied offen und erfolgt formlos. Die Landesparteiparlamente entsenden Beisitzer in das Bundesparteiparlament. Diese haben ein Rede-, aber kein Abstimmungsrecht.
2. Das Landesparteiparlament ratifiziert die programmatischen und politischen Ziele des entsprechenden Landesverbands. Es formuliert offizielle Positionen des Landesverbands zu landesspezifischen oder landesaktuellen Themen. Seine Mitglieder werden durch den Landesverband aus der Mitte seiner Mitglieder gewählt. Eine Bewerbung steht jedem Mitglied offen und erfolgt durch schriftliche Erklärung gegenüber dem Vorsitzenden des Landesverbands. Je 10 Kreisverbände entsenden einen Beisitzer in das Bundesparteiparlament. Dabei müssen diese Kreisverbände in einem geografischen oder programmatischen Zusammenhang stehen.
3. Das Kreisparteiparlament formuliert die programmatischen und politischen Ziele des Kreisverbandes. Es legt die Ziele und Anliegen des Kreisverbandes fest. Seine Mitglieder werden zunächst durch den Kreisverband aus der Mitte seiner Mitglieder gewählt. Jeder Kreisverband kann nach eigener Maßgabe Vertreter der Zivilgesellschaft oder Persönlichkeiten von regionaler Bedeutung in sein Kreisparteiparlament berufen. Dabei muss die Anzahl der Abgeordneten, die gleichzeitig Mitglieder der Partei sind, stets über der Hälfte der Abgeordneten liegen.

Einberufung der Online Mitgliederversammlungen:    Die Partei beruft in allen Gebietsverbänden auf allen Gliederungsebenen ständig tagende Online Mitgliederversammlungen ein. Die Online Mitgliederversammlungen erörtern programmatische und organisatorische Fragen, tagesaktuelle Themen und gegenwärtige sowie zukünftige Projekte der Partei des Fortschritts. Sie haben hinsichtlich dieser Punkte ein Petitionsrecht an den Parteisprecher, den Bundesvorstand sowie die Vorstände der Gebietsverbände und die Parteiparlamente auf allen Ebenen. Beschließt die Mehrheit einer Online-Mitgliederversammlung eines Gebietsverbands einen programmatischen Vorschlag, so hat das für diesen Gebietsverband zuständige Parteiparlament den Vorschlag als eigene Beschlussvorlage zu behandeln. Dasselbe gilt für programmatische Vorschläge der Bundes-Online-Mitgliederversammlung hinsichtlich des Bundesparteiparlaments.

Arbeitsweise der Partei und ihrer Organe:     Die Partei und ihre Organe leisten ihre politische Arbeit grundsätzlich unter Ausschöpfung aller technischen Möglichkeiten und im Geiste einer autonomen Kooperation. Ihre Organe treten, sofern dies rechtlich und tatsächlich möglich ist und nicht Gründe der Effektivität oder des Zusammenwachsens der Partei entgegen stehen auch online zusammen. Die Entscheidung, ob eine Versammlung, eine sonstige Zusammenkunft oder sonstige Arbeitsschritte online oder in Präsenz durchgeführt werden, obliegt im Einzelfall dem jeweiligen Organ.

Veranstaltungsleitung:    Die in diesem Absatz der Satzung beschriebenen Organe wählen auf ihren Veranstaltungen eine für diese Veranstaltung zuständige oder ständig zuständige Veranstaltungsleitung. Die Veranstaltungsleitung ist für die Ordnung innerhalb des Organs, der Veranstaltung und für die Durchführung der aktuellen Tagesordnung zuständig. Sobald ein dauerhaft eingerichtetes Organ der Partei zum ersten Mal zusammentritt, gibt es sich durch Beschluss per qualifizierter Mehrheit von 2/3 der abgegebenen Stimmen eine Geschäftsordnung. Änderungen einer einmal erlassenen Geschäftsordnung bedürfen ebenfalls 2/3 der abgegebenen Stimmen in einer Sitzung des entsprechenden Organs.

Satzungshoheit der Bundespartei:    Untergliederungen der Partei im Sinne von Landesverbänden, Kreisverbänden oder Ortsverbänden sind nur insofern berechtigt sich Satzungen zu geben die von dieser Satzung abweichen, wie es aufgrund ihrer regionalen und organisatorischen Eigenheit zwingend notwendig ist. Der Wesensgehalt ihrer Satzungen, insbesondere die Arbeitsweise, die Beschlussfassung und die Verteilung der Organe so wie ihrer Kompetenzen müssen mit dieser Satzung übereinstimmen.

Gründung eines Gebietsverbandes:    Über die Gründung eines Gebietsverbands oder einer sonstigen Untergliederung entscheidet der nächsthöchste Parteitag. Bei Landesverbänden ist dies der Bundesparteitag. Bei Kreis- und Ortsverbänden der Landesparteitag des jeweiligen Landesverbands. Voraussetzung ist, dass eine Zahl von 20 Mitgliedern bei Landesverbänden und 7 Mitgliedern bei Kreis- beziehungsweise Ortsverbänden den Willen äußert einen solchen Gebietsverband zu gründen.

Öffentlichkeitsgrundsatz:    Die Organe der Partei tagen grundsätzlich öffentlich. Hinsichtlich eines Vorstands des Bundesverbandes, eines Landesverbandes oder eines Kreis- beziehungsweise Ortsverbandes meint öffentlich, dass alle Parteimitglieder des entsprechenden Gebietsverbandes bei den Sitzungen ein Anwesenheitsrecht haben. Hinsichtlich aller weiteren Organe meint öffentlich, dass eine Anwesenheit auch Bürger zusteht, sofern diese sich vorher beim für den Gebietsverband zuständigen Vorstand angemeldet haben. Die Organe können in ihren Geschäftsordnungen Regelungen treffen, um den Öffentlichkeitsgrundsatz verhältnismäßig und bei Vorliegen triftiger Gründe einzuschränken oder einzelne aufgrund dieses Absatzes Anwesende von der Veranstaltung auszuschließen.

## Parteiparlamente

Grundsätze des Parteiparlamentes:   Die Parteiparlamente können als ordentliche Parteiparlamente oder als digitale Parteiparlamente als einheitliches Organ zusammentreten. Durch eigenen Beschluss können sie auch gleichzeitig räumlich und digital arbeiten, solange die Einheitlichkeit des Organs und der zu treffenden Beschlüsse dadurch gewahrt bleibt.

Ordentliche Parteiparlamente:    sind Parteiparlamente, die in räumlicher und zeitlicher Hinsicht an einem Ort in Präsenz zusammentreten. Sie sind als vorrangige Beschlussorgan der Partei hinsichtlich aller programmatischen Fragen. Sie tagen ab ihrem ersten Zusammentreten regelmäßig wenigstens einmal wöchentlich. Sie behandeln primär in den Arbeitskreisen erstellte Programmvorschläge und behandeln diese als eigene Beschlussvorlage. Sie können durch wenigstens 25% ihrer Abgeordneten auch eigene programmatische Beschlussvorlagen einbringen. Das Nähere hinsichtlich Einberufung, Verfahren, Zusammensetzung, Abstimmungsmodus und innerer Ordnung regelt abseits der Bestimmungen dieser Satzung ihre Geschäftsordnung. Die Geschäftsordnung geben sich die Parteiparlamente bei ihrem ersten Zusammentreten selbst. Sie nach Maßgabe des Art. 6a (7) dieser Satzung zunächst dem Parteivorsitzenden zur Gegenzeichnung zuzuleiten.

Online-Parteiparlamente:    sind Parteiparlamente, die in räumlicher und zeitlicher Hinsicht durch digitale Telekommunikationsmittel zusammentreten. Es gelten die Bestimmungen über die ordentlichen Parteiparlamente entsprechend.

Gliederung der Parteiparlamente:    Die Parteiparlamente gliedern sich entsprechend der Bundespartei und ihrer Gebietsverbände.
1. Das Bundesparteiparlament ratifiziert die programmatischen und politischen Ziele des Bundesverbands. Es formuliert offizielle Positionen der Partei zu gesamtgesellschaftlichen oder tagesaktuellen Zielen. Seine Mitglieder werden durch den Bundesverband auf schriftliche Bewerbung berufen. Eine Bewerbung steht jedem Mitglied offen und erfolgt formlos. Die Landesparteiparlamente entsenden Beisitzer in das Bundesparteiparlament. Diese haben ein Rede-, aber kein Abstimmungsrecht.
2. Das Landesparteiparlament ratifiziert die programmatischen und politischen Ziele des entsprechenden Landesverbands. Es formuliert offizielle Positionen des Landesverbands zu landesspezifischen oder landesaktuellen Themen. Seine Mitglieder werden durch den Landesverband aus der Mitte seiner Mitglieder gewählt. Eine Bewerbung steht jedem Mitglied offen und erfolgt durch schriftliche Erklärung gegenüber dem Vorsitzenden des Landesverbands. Je 10 Kreisverbände entsenden einen Beisitzer in das Bundesparteiparlament. Dabei müssen diese Kreisverbände in einem geografischen oder programmatischen Zusammenhang stehen.
3. Das Kreisparteiparlament formuliert die programmatischen und politischen Ziele des Kreisverbandes. Es legt die Ziele und Anliegen des Kreisverbandes fest. Seine Mitglieder werden zunächst durch den Kreisverband aus der Mitte seiner Mitglieder gewählt. Jeder Kreisverband kann nach eigener Maßgabe Vertreter der Zivilgesellschaft oder Persönlichkeiten von regionaler Bedeutung in sein Kreisparteiparlament berufen. Dabei muss die Anzahl der Abgeordneten, die gleichzeitig Mitglieder der Partei sind, stets über der Hälfte der Abgeordneten liegen.

Beschlussfähigkeit des Parteiparlamentes:    Ordentliche Parteiparlamente sind nur beschlussfähig, wenn eine Anzahl von Abgeordneten vertreten ist, die entweder die Mitglieder der Bundespartei oder des entsprechenden Gebietsverbandes vollumfänglich repräsentiert. Ansonsten sind Parteiparlamente beschlussfähig, sofern wenigstens die Hälfte der durch die Partei gewählten Abgeordneten räumlich oder digital in einer Sitzung anwesend sind. Bei Abweichung von Satz 2 kann sich ein Parteiparlament durch Beschluss mit einer Mehrheit von 2/3 der anwesenden Abgeordneten für beschlussfähig erklären. Der Parteiparlamentspräsident ist für die Einberufung der Parteiparlamente, die Einhaltung der Satzung und die ordentliche Durchführung ihrer Sitzungen zuständig.

Beschlussfähigkeit des Online-Parteiparlament:    Online-Parteiparlamente sind nur beschlussfähig, wenn eine Mindestanzahl an Abgeordneten anwesend ist. Bis zum 31.12.2021 beträgt diese Mindestanzahl 99 Abgeordnete. Bis zum 31.12.2022 beträgt diese Mindestanzahl 149 Mitglieder. Nach dem 31.21.2022 wird die Mindestanzahl durch Beschluss eines ordentlichen Parteitags angepasst.

Meinungsbilder durch beschlussunfähigen Parteiparlamente:    Beschlussunfähige Parteiparlamente können trotz ihrer Beschlussunfähigkeit Beschlüsse über veranstaltungsrechtliche Entscheidungen fällen. Parteiparlamente können unabhängig von ihrer Beschlussfähigkeit jederzeit unverbindliche Meinungsbilder beschließen.

Gegenzeichnung der Beschlüssen:    Nach Beschlussfassung haben die Parteiparlamente aller Gliederungen und unabhängig von der Form ihres Zusammentretens ihren Beschluss unverzüglich dem Parteivorsitzenden zur Gegenzeichnung zuzuleiten. Ein Beschluss erlangt erst durch die Gegenzeichnung volle Wirksamkeit. Der Parteivorsitzende kann die Gegenzeichnung verweigern, wenn der Beschluss nicht nach den Bestimmungen dieser Satzung zustande gekommen ist, mit dem bestehenden Parteiprogramm im Widerspruch steht oder der Partei im politischen Wettbewerb erheblichen Schaden zufügen würde. Verweigert der Parteivorsitzende die Gegenzeichnung, hat er den Beschluss binnen einer Woche nach Zuleitung an das Parteiparlament zurückzuüberweisen. Überweist der Parteisprecher einen Beschluss nicht unter Angabe der Verweigerung der Gegenzeichnung nicht innerhalb dieser Frist an das Parteiparlament zurück, gilt die Gegenzeichnung als erfolgt. Das Parteiparlament kann den Widerspruch der Gegenzeichnung mit einer Mehrheit von 2/3 der Abgeordneten des Parteiparlaments überstimmen. Überstimmt das Parteiparlament den Widerspruch der Gegenzeichnung, beschränkt sich die anschließende Prüfungskompetenz des Parteivorsitzenden auf die Einhaltung des Beschlussverfahrens nach der Satzung.

Zustimmung zur Speicherung der Abstimmungsergebnisse:    Abstimmungen der Parteiparlamente erfolgen durch freie und gleiche Wahl. Die Abstimmungsergebnisse werden in einem Parteiinternen System gespeichert und den Mitgliedern zugänglich gemacht. Durch die Annahme eines Mandats für ein Parteiparlament, erklärt sich ein Mitglied mit der dahingehenden Speicherung seiner personenbezogenen Daten über die Dauer der Mitgliedschaft hinaus einverstanden.

Voraussetzung der digitalen Wahl:    Abstimmungen der Online-Parteiparlamente haben über geeignete technische Systeme stattzufinden, welche eine freie und gleiche Wahl gewährleisten. Dabei kann die Partei sich eigenen oder fremden Systemen bedienen, solange diese die Anforderungen des S. 1 erfüllen.

# Zusammensetzung und Befugnisse des Vorstands und der übrigen Organe

Vorstand:    Zur Leitung und der Führung der Geschäfte des Bundesverbands nach den Beschlüssen der Parteitage und Parteiparlamente wird ein Vorstand gebildet.
1. Zur Durchführung seiner Aufgaben tritt der Vorstand mindestens einmal monatlich zusammen. Er fasst Beschluss über konkrete Maßnahmen zur Durchführung der Beschlüsse der Parteitage und Parteiparlamente und berichtet diesen darüber. Er koordiniert die Umsetzung der Parteipolitik in den Gebietsverbänden und die Kooperation zwischen Gebietsverbänden und Bundesverband. Er überwacht nach den Vorschriften des PartG und des §12 dieser Satzung die Finanzen der Partei.
2. Der Vorstand der Partei besteht aus dem Vorsitzenden, dem stellvertretenden Vorsitzenden, dem Schatzmeister der Partei, einem Schriftführer und nicht-stimmberechtigen Beisitzern aller Landesverbände.
3. Der Parteivorstand wird aus der Mitte der Mitglieder der Partei gewählt. Jedes Mitglied der Partei kann sich zur Wahl als Vorstandsmitglied aufstellen lassen. Die Bundes-, Landes- und Parteiparlamente können jeweils eigene Kandidaten benennen. Voraussetzung für die Aufstellung eines Kandidaten für die Wahl des Vorstands, ist eine gewisse Dauer und Intensität an Engagement für die Politik und Ziele der Partei.
4. Der Vorstand kontrolliert die Arbeit des Parteisprechers. Er beruft zusammen mit dem Parteivorsitzenden die Vorstände der Landes- und Kreisverbände.
5. Der Vorstand erarbeitet keine eigenen politischen Positionen. Seine Aufgabe hinsichtlich programmatischer Fragen besteht einzig darin die durch den innerparteilichen parlamentarischen Prozess erarbeiteten Positionen umzusetzen und nach außen zu vertreten. Dies berührt nicht die Rechte der einzelnen Vorstandsmitglieder als Mitglieder der Partei.

Parteivorsitzender:    Die Partei hat einen Parteivorsitzenden. In Anerkennung des basisdemokratischen Charakters der PdF trägt dieser nach außen den Titel Parteisprecher. Der Parteisprecher besitzt Richtlinienkompetenz und führt den Bundesverband. Weiterhin vertritt er die Partei nach außen und wirkt an der äußerlichen Formulierung der Grundsätze und Ziele der Partei mit. Der Parteivorsitzende wird durch den Vorstand aus den Mitgliedern der Partei gewählt. Der Parteivorsitzende ist zunächst auch Mitglied des Vorstands.
1. Das Amt des Parteivorsitzenden kann auch in Form einer Doppelspitze ausgeübt werden. Dabei müssen die beiden das Amt bekleidenden Personen wenigstens zwei verschiedenen Geschlechtern angehören. Hat die Partei eine Doppelspitze gewählt, haben beide Amtsträger alle organisatorischen oder programmatischen Fragen miteinander abzustimmen und gegenüber der Partei sowie nach außen stets mit einer geschlossenen Position aufzutreten.
2. Die Richtlinienkompetenz des Parteivorsitzenden bezieht sich auf alle organisatorischen und programmatischen Fragen. Er hat dazu ein Petitionsrecht an alle übrigen Organe der Partei. Diese haben über seine Vorschläge nach den in dieser Satzung festgelegten Bestimmungen Beschluss zu fassen.
3. Der Parteivorsitzende kann zur Erfüllung seiner Aufgaben Unterorgane bilden und diese personell besetzten. Er hat dabei grundsätzlich Mitglieder der Partei zu wählen, kann falls triftige Gründe dafür sprechen Stellen aber auch extern besetzten.
4. Der Parteivorsitzende trägt Sorge für das ordentliche und satzungsgemäße Zusammentreten und die satzungsgemäße Durchführung der Aufgaben der Organe der Partei. Er kann dazu von allen Organen der Parteien Auskünfte verlangen und diese schriftlich zur Erfüllung ihrer Aufgaben oder zur Einhaltung der Satzung auffordern.

Parteiparlament:    Die Parteiparlamente der entsprechenden Gliederung verfügen über die in Art. 6 der Satzung formulierten Befugnisse und Kompetenzen. Sie sind in ihrer Arbeit frei von Einflussnahme durch den Vorstand oder den Vorsitzenden.

Neuschaffung weiterer Organe:    Die Partei kann zur Erfüllung ihrer Ziele durch ihren Vorsitzenden oder ihren Vorstand weitere Organe schaffen. Die Aufgaben und Kompetenzen dieser Organe können sich mit jenen der in dieser Satzung genannten überschneiden. Im Konfliktfall entscheidet der Parteivorstand.

# Beschlussfassungen durch die Mitglieder- und Vertreterversammlungen

Des Vorstands:    Der Vorstand trifft seine Entscheidung durch die Mehrheit seiner Mitglieder.

Des Parteiparlamentes:    Die Parteiparlamente treffen ihre Entscheidungen durch die Mehrheit der abgegebenen Stimmen.

Der Landes- oder Kreisverbänden:    Von den Landes- oder Kreisverbänden geschaffene Organe treffen ihre Entscheidungen nach deren Maßgabe.

Weitere Organe:    Alle weiteren Mitglieder- oder Vertreterversammlungen oder sonstige durch den Bundesverband geschaffene Organe treffen ihre Entscheidungen nach der Mehrheit der abgegebenen Stimmen, sofern bei ihrer Schaffung nicht etwas anderes bestimmt wird.

Möglichkeit elektronischer Wahlen:     Alle Parteiinternen Wahlverfahren, welche nach den gesetzlichen Vorgaben nur von Mitglieder-/Vertreterversammlungen durchgeführt werden dürfen, können, falls eine besondere Situation dies erfordert, auch im Rahmen von Onlineveranstaltung (etwa über Videokommunikationsdienste) mit anschließender Briefwahl zur Bestätigung durchgeführt werden. Die Feststellung einer besonderen Situation im Sinne dieses Absatzes obliegt dem Vorstand. Zur Ausgestaltung des Verfahrens ermächtigt der Vorstand durch seine Feststellung den Parteisprecher zur Auswahl des Verfahrens im Rahmen mit den zum Zeitpunkt der Feststellung geltenden gesetzlichen Bestimmungen.

Digitale Mitglieder- und Vertreterversammlungen:     Treffen ihre Entscheidungen nach der Mehrheit der abgegebenen Stimmen.

# Voraussetzung, Form und Frist der Einberufung der Mitglieder- und Vertreterversammlungen sowie Beurkundung der Beschlüsse

Aufgabe der Parteitage:    Die ordentlichen Mitglieder- und Vertreterversammlungen (Parteitage) treten als Mitgliederversammlungen zusammen. Sie entscheiden über den Beschluss der Satzung, die Beitragsordnung, die Schiedsgerichtsordnung, die Auflösung sowie Verschmelzung mit anderen Parteien und die Wahl des Vorstands. Außerdem nehmen sie mindestens alle zwei Jahre einen Tätigkeitsbericht des Vorstands entgegen und fassen hierüber Beschluss.

Einberufung:    Die Parteitage werden durch den Bundesvorstand durch ordentlichen Beschluss wenigstens einmal alle 3 Monate einberufen. Eine häufigere Einberufung ist möglich. Voraussetzung ist, dass der Vorstand eine für die Parteiarbeit grundlegende Frage als zu klären ansieht oder 10% der Mitglieder der Partei ein entsprechendes Begehren an den Vorstand richten. Die Einberufung hat schriftlich und elektronisch wenigstens 2 Wochen vor dem Parteitag zu erfolgen. In dieser Zeit hat der Vorstand sicherzustellen, dass durch geeignete technische Mittel und auf allen Kommunikationskanälen für alle Mitglieder der Partei die Möglichkeit besteht, Anträge zur Tagesordnung des Parteitags einzureichen. Die Beschlüsse der Parteitage sind durch den Schriftführer, den Schriftführer des Parteitags, den Vorstand und den Parteivorsitzenden durch Unterschrift zu beurkunden. Jedes Parteimitglied muss spätestens eine Woche nach dem Parteitag schriftlich oder elektronisch über dessen Beschlüsse unterrichtet werden.

Sonderparteitage:    Für außerordentliche Mitglieder- und Vertreterversammlungen (Sonderparteitage) gelten die Absätze 1 und 2 entsprechend. Ihre Funktion besteht dabei allerdings vorrangig in der tagesaktuellen Festlegung der politischen Positionen der Partei. Sonderparteitage sind nicht zu Beschlüssen nach §9 III-V PartG berechtigt.

Tagung der Parteiparlamente:    Die Parteiparlamente tagen nach ihrer ersten Einrichtung als dauerhafte Organe der Partei. Sie werden durch schriftlichen Beschluss des entsprechenden Verbands einberufen. Sie treten spätestens einen Monat nach ihrer Einberufung zusammen.

Weitere Versammlungen:    Weitere Mitglieder- und Vertreterversammlungen können durch den entsprechenden Verband durch schriftlichen Beschluss einberufen werden.

Protokollpflicht der Organe:    Beschlüsse aller Parteiorgane, sowie aller weiteren Versammlungen oder Gremien der Parteien müssen schriftlich protokolliert werden. Sie werden innerhalb von zwei Wochen zur Beurkundung an den Bundesvorstand geschickt. Dieser nimmt innerhalb von einer Woche schriftlich Kenntnis.

Schlussbestimmungen:    Weitere Frist- oder Formerfordernisse bestehen nicht.

## Digitale Mitglieder- und Vertreterversammlungen

Online-Zusammentritt:    Die ordentlichen digitalen Mitglieder- und Vertreterversammlungen (Online-Parteitage) treten als Mitgliederversammlungen auf digitalem Wege zusammen. Sie können im Rahmen der geltenden gesetzlichen Bestimmungen und dieser Satzung über dieselben Themen Beschluss fassen wie die ordentlichen Parteitage.

Einberufung des Online-Parteitages:    Die Online-Parteitage werden durch den Bundesvorstand oder den Vorstand der zuständigen Gebietskörperschaft einberufen. Eine Einberufung eines Online-Parteitags erfüllt das Erfordernis der Häufigkeit der Einberufung ordentlicher Parteitage nach Art. 9 II der Satzung. Eine häufigere Einberufung ist möglich. Ein Online-Parteitag kann einberufen werden, wenn der zuständige Vorstand eine Frage der Parteiarbeit als zu klären ansieht oder 10% der Mitglieder der zuständigen Gebietskörperschaft ein entsprechendes Begehren an den Vorstand richtet und die Durchführung eines Online-Parteitags aus organisatorischen oder administrativen Gründen vorteilhaft gegenüber der Durchführung eines ordentlichen Parteitags ist. Die Einberufung hat schriftlich oder elektronisch oder per Aushang wenigstens 2 Wochen vor dem Parteitag in der Form zu erfolgen, dass alle Mitglieder unterrichtet werden. Die Beschlüsse der Parteitage sind durch den Schriftführer des Online-Parteitags durch persönliche handschriftliche Unterschrift zu beurkunden. Der Schriftführer der Partei, ein Mitglied des Vorstands und der Parteivorsitzende haben dem Schriftführer des Online Parteitags zur Beurkundung eine Unterschriftenvollmacht hinsichtlich der Beschlüsse des Online Parteitags zu erteilen. Jedes Parteimitglied muss spätestens eine Woche nach dem Online- Parteitag elektronisch über dessen Beschlüsse unterrichtet werden.

Auswahl technischer Systeme:     Abstimmungen der Online-Parteitage haben über geeignete technische Systeme stattzufinden, welche eine freie und gleiche Wahl gewährleisten. ist durch Gesetz eine Wahl als geheim durchzuführen, muss sich die Geeignetheit der ausgewählten Systeme auch darauf erstrecken. Dabei kann die Partei sich eigenen oder fremden Systemen bedienen, solange diese die Anforderungen des S. 1 erfüllen. Ist durch Gesetz für eine Abstimmungsfrage eine Schlussabstimmung per Brief oder Urnenwahl durchzuführen, hat diese im Anschluss an den Online-Parteitag unverzüglich zu erfolgen.

Schlussbestimmungen:    Weitere Frist- oder Formerfordernisse bestehen nicht.

# Gebietsverbände und Organe, die zur Einreichung von Wahlvorschlägen für Wahlen zu Volksvertretungen befugt sind

Gebietsverbände:    Alle Gebietsverbände sind zur Einreichung von Wahlvorschlägen für Wahlen zu Volksvertretungen berechtigt.

Gebietsparteiparlamente:     Alle Gebietsparteiparlamente sind zur Einreichung von Wahlvorschlägen für Wahlen zu Volksvertretungen berechtigt.

Vorstand:    Der Vorstand und der Vorsitzende sind zur Einreichung von Wahlvorschlägen zu Volksvertretungen berechtigt.

Entscheidungsgewalt:    Über alle Wahlvorschläge entscheidet ein dazu durch den Vorstand einberufenes Organ. Diesem sind Beisitzer aus allen Gebietsverbänden und dem Bundesverband zugeordnet.

# Urabstimmung der Mitglieder und Verfahren bei Auflösung der Partei oder eines Gebietsverbands oder der Verschmelzung der PdF mit anderen Parteien

Durchführung einer Urabstimmung:    Die Partei des Fortschritts nach einem darauf gerichteten Beschluss des Bundesparteitags nur durch Beschluss von zwei Dritteln ihrer Mitglieder aufgelöst werden. Zur Urabstimmung hat der Vorstand alle Parteimitglieder aufzufordern und konkret Datum, Uhrzeit und Ort der Urabstimmung mindestens zwei Monate vorher mitzuteilen. Die Abstimmung erfolgte durch gleiche, geheime und freie Wahl. Parteimitgliedern, die aus persönlichen Gründen nicht zur Urabstimmung erscheinen können, ist die Möglichkeit der Abstimmung per Brief einzuräumen.
Verschmelzen mit anderen Parteien:    Die Partei des Fortschritts kann durch Beschluss von zwei Dritteln ihrer Mitglieder mit einer anderen Partei verschmelzen. Für die Abstimmung gilt Absatz 1 entsprechend.

# Besondere Verantwortung von Amt- und Mandatsträgern

Verantwortung der Amtsträger:    Amtsträger parteiinterner Ämter haben für die Dauer ihrer Amtszeit die besondere Verantwortung ihres Amtes zu beachten. Sie dürfen weder intern noch öffentlich Äußerungen tätigen oder Maßnahmen vollziehen, welche im Widerspruch zu Satzung, Programm oder Zielsetzung der Partei stehen. Insbesondere haben sie alle formellen und materiellen Bestimmungen der Satzung stets und in jeder Hinsicht zu beachten. Sie haben ein Augenmerk auf die effiziente Umsetzung ihrer Verantwortung zur Förderung der Anliegen der Partei zu legen und die Positionen der Partei vorrangig vor ihrer persönlichen Meinung zu vertreten; das Einbringen der eigenen politischen Meinung durch Nutzung des Rede-, Antrags- und Stimmrechts innerhalb der Partei ist hiervon ausgenommen. Verstößt ein Amtsträger schuldhaft und in zurechenbarer Weise nicht nur unerheblich gegen die Bestimmungen dieses Absatzes, kann die Partei deshalb satzungsrechtliche Ordnungsmaßnahmen gegen ihn durchführen.

Pflichten der Mandatsträger:    Mitglieder der Partei, die über ein Mandat in einem Parlament, einer Verwaltung, einer Stiftung oder einem Beirat verfügen, sollen die besondere Verantwortung ihres Mandats unter Beachtung der verfassungsrechtlichen und einfachgesetzlichen Freiheit ihres Mandats für die Umsetzung der politischen Ziele der Partei zu nutzen. Sie sollen dabei vor allem auf die Umsetzung der programmatischen Ziele der Partei hinzuwirken und ihr Abstimmungsverhalten danach ausrichten. In Anerkennung des basisdemokratischen Charakters der Partei des Fortschritts, hat sich jedes Mitglied vor und während der Erlangung eines Mandats im Klaren darüber zu sein, dass es gegebenenfalls auch Positionen zu vertreten hat, die zu der jeweiligen persönlichen Einstellung im Widerspruch stehen. Ein Mandatsträger der Partei das diesen Konflikt im Einzelfall als unlösbar empfindet, ist gehalten das entsprechende Mandat niederzulegen.

# Finanzordnung

## Zuständigkeit für die Parteifinanzen

Schatzmeister:    Zur Verwaltung der Finanzmittel der Partei wählt der Parteitag einen Schatzmeister. Der Schatzmeister ist Mitglied des Vorstands. Der Schatzmeister eröffnet mindestens ein Konto und ist zu dessen gewissenhafter Führung berechtigt und verpflichtet. Falls dies zur effektiven Parteiarbeit nötig ist, kann der Schatzmeister auch mehrere Konten eröffnen. Er kann seine Verfügungsgewalt über diese Konten mit anderen Parteimitgliedern teilen. Diese müssen dafür durch ihn bestellt und durch den Vorstand per Beschluss bestätigt werden. Für die Gebietsverbände gelten diese Vorschriften entsprechend.

Vorlage des Rechenschaftsberichts:    Der Vorstand der Partei hat durch seinen Schatzmeister über die Herkunft und die Verwendung der Mittel sowie über das Vermögen der Partei zum Ende des Kalenderjahres (Rechnungsjahr) in einem Rechenschaftsbericht wahrheitsgemäß und nach bestem Wissen und Gewissen öffentlich Rechenschaft zu geben.

Vorgaben für die Parteifinanzen:    Der Schatzmeister des Bundesverbandes legt bei jedem Parteitag des Bundesverbandes die Einnahmen und Ausgaben sowie die allgemeine Finanzlage des Bundesverbandes dar. Entsprechendes gilt für die Schatzmeister der Landesverbände. Die Schatzmeister der Landesverbände arbeiten der Bundesschatzmeisterei zu. Bundesverband und Landesverbände verwenden ihr Parteivermögen in gegenseitiger Abstimmung. Der Bundesverband kann einem Landesverband zur Förderung landespolitischer Ziele oder zur Förderung bundespolitischer Ziele in dem entsprechenden Landesverband Finanzmittel zuwenden. Im Falle einer solchen Zuwendung hat der Bundesschatzmeister über die Verwendung insofern Aufsicht, als dass er deren satzungsgemäße Verwendung überprüfen kann und sicherstellt, dass die Finanzmittel nicht grob zweckwidrig verwendet werden.

## Rechenschaftsbericht

Beratung des Berichtes:    Der Rechenschaftsbericht soll vor der Zuleitung an den Präsidenten des Deutschen Bundestages im Vorstand der Partei beraten werden.

Zuständigkeit:    Der Bundesvorstand der Partei sowie die Vorstände der Landesverbände und die Vorstände der den Landesverbänden vergleichbaren Gebietsverbände sind jeweils für ihre Rechenschaftslegung verantwortlich. Ihre Rechenschaftsberichte werden vom Vorsitzenden und einem vom Parteitag gewählten für die Finanzangelegenheiten zuständigen Vorstandsmitglied oder von einem für die Finanzangelegenheiten nach der Satzung zuständigen Gremium gewählten Vorstandsmitglied unterzeichnet. Diese für die Finanzangelegenheiten zuständigen Vorstandsmitglieder versichern mit ihrer Unterschrift, dass die Angaben in ihren Rechenschaftsberichten nach bestem Wissen und Gewissen wahrheitsgemäß gemacht worden sind.

Bericht der Gesamtpartei:    Der Rechenschaftsbericht der Gesamtpartei wird von einem vom Parteitag gewählten für die Finanzangelegenheiten zuständigen Vorstandsmitglied des Bundesvorstandes oder von einem für die Finanzangelegenheiten nach der Satzung zuständigen Gremium gewählten Mitglied des Bundesvorstandes zusammengefügt und unterzeichnet.

Prüfung des Berichtes:    Der Rechenschaftsbericht muss von einem Wirtschaftsprüfer oder einer Wirtschaftsprüfergesellschaft nach den Vorschriften der §§ 29 -31 PartG geprüft werden.
1. Die Prüfung nach erstreckt sich auf die Bundespartei, ihre Landesverbände sowie nach Wahl des Prüfers auf mindestens zehn nachgeordnete Gebietsverbände. In die Prüfung ist die Buchführung einzubeziehen. Die Prüfung hat sich darauf zu erstrecken, ob die gesetzlichen Vorschriften beachtet worden sind. Die Prüfung ist so anzulegen, dass Unrichtigkeiten und Verstöße gegen die gesetzlichen Vorschriften bei gewissenhafter Berufsausübung erkannt werden.
2. Der Prüfer kann von den Vorständen und den von ihnen dazu ermächtigten Personen alle Aufklärungen und Nachweise verlangen, welche die sorgfältige Erfüllung seiner Prüfungspflicht erfordert. Es ist ihm insoweit auch zu gestatten, die Unterlagen für die Zusammenstellung des Rechenschaftsberichts, die Bücher und Schriftstücke sowie die Kassen- und Vermögensbestände zu prüfen.
3. Der Vorstand des zu prüfenden Gebietsverbandes hat dem Prüfer schriftlich zu versichern, dass in dem Rechenschaftsbericht alle rechenschaftspflichtigen Einnahmen, Ausgaben und Vermögenswerte erfasst sind. Auf die Versicherung der Vorstände nachgeordneter Gebietsverbände kann Bezug genommen werden. Es genügt die Versicherung des für die Finanzangelegenheiten zuständigen Vorstandsmitgliedes.
4. Das Ergebnis der Prüfung ist in einem schriftlichen Prüfungsbericht niederzulegen, der dem Vorstand der Partei und dem Vorstand des geprüften Gebietsverbandes zu übergeben ist. Sind nach dem abschließenden Ergebnis der Prüfung durch das zuständige Parteiorgan keine Einwendungen zu erheben, so hat der Prüfer durch einen Vermerk zu bestätigen, dass nach pflichtgemäßer Prüfung aufGrund der Bücher und Schriften der Partei sowie der von den Vorständen erteilten Aufklärungen und Nachweise der Rechenschaftsbericht in dem geprüften Umfang den Vorschriften des PartG entspricht. Sind Einwendungen zu erheben, so hat der Prüfer in seinem Prüfungsvermerk die Bestätigung zu versagen oder einzuschränken. Die geprüften Gebietsverbände sind im Prüfungsvermerk namhaft zu machen. Der Prüfungsvermerk ist auf dem einzureichenden Rechenschaftsbericht anzubringen und in vollem Wortlaut mitzuveröffentlichen.

Vorgehen bei Fehlern:    Werden durch den Wirtschaftsprüfer oder ein Parteiorgan Fehler im Rechenschaftsbericht festgestellt, ist dieser zu Berichtigen und bei der zuständigen Behörde neu einzureichen. Dies ist von einem Wirtschaftsprüfer oder einer Wirtschaftsprüfungsgesellschaft durch einen Vermerk zu bestätigen.

Inhalt:    Der Rechenschaftsbericht besteht aus einer Ergebnisrechnung auf der Grundlage einer den Vorschriften des PartG entsprechenden Einnahmen- und Ausgabenrechnung, einer damit verbundenen Vermögensbilanz sowie einem Erläuterungsteil. Er gibt unter Beachtung der Grundsätze ordnungsmäßiger Buchführung entsprechend den tatsächlichen Verhältnissen Auskunft über die Herkunft und Verwendung der Mittel sowie über das Vermögen der Partei.

Geltende Vorschriften:    Die für alle Kaufleute geltenden handelsrechtlichen Vorschriften über die Rechnungslegung, insbesondere zu Ansatz und Bewertung von Vermögensgegenständen, sind entsprechend anzuwenden, soweit das PartG nichts anderes vorschreibt. Rechnungsunterlagen, Bücher, Bilanzen und Rechenschaftsberichte sind zehn Jahre aufzubewahren. Die Aufbewahrungsfrist beginnt mit Ablauf des Rechnungsjahres.

Gesamtheit aller Berichte:    In den Rechenschaftsbericht der Gesamtpartei sind die Rechenschaftsberichte jeweils getrennt nach Bundesverband und Landesverband sowie die Rechenschaftsberichte der nachgeordneten Gebietsverbände je Landesverband aufzunehmen. Die Landesverbände und die ihnen nachgeordneten Gebietsverbände haben ihren Rechenschaftsberichten eine lückenlose Aufstellung aller Zuwendungen je Zuwender mit Namen und Anschrift beizufügen. Der Bundesverband hat diese Aufstellungen zur Ermittlung der jährlichen Gesamthöhe der Zuwendungen je Zuwender zusammenzufassen. Die Landesverbände haben die Teilberichte der ihnen nachgeordneten Gebietsverbände gesammelt bei ihren Rechenschaftsunterlagen aufzubewahren.
\\
\\
Einnahmerechnung:
1. Mitgliedsbeiträge,
2. Mandatsträgerbeiträge und ähnliche regelmäßige Beiträge,
3. Spenden von natürlichen Personen,
4. Spenden von juristischen Personen,
5. Einnahmen aus Unternehmenstätigkeit,
	1. Einnahmen aus Beteiligungen,
6. Einnahmen aus sonstigem Vermögen,
7. Einnahmen aus Veranstaltungen, Vertrieb von Druckschriften und Veröffentlichungen und sonstiger mit Einnahmen verbundener Tätigkeit,
8. staatliche Mittel,
9. sonstige Einnahmen,
10. Zuschüsse von Gliederungen und
11. Gesamteinnahmen nach den Nummern 1 bis 10.

Die Ausgaberechnung umfasst:    
1. Personalausgaben,
2. Sachausgaben
	1. des laufenden Geschäftsbetriebes,
	2. für allgemeine politische Arbeit,
	3. für Wahlkämpfe,
	4. für die Vermögensverwaltung einschließlich sich hieraus ergebender Zinsen,
	5. sonstige Zinsen,
	6. Ausgaben im Rahmen einer Unternehmenstätigkeit,
	7. sonstige Ausgaben,
3. Zuschüsse an Gliederungen und
4. Gesamtausgaben nach den Nummern 1 bis 3.

Die Vermögensbilanz umfasst:
1. Besitzposten:
	1. Anlagevermögen:
		1. Sachanlagen:
			I. Haus- und Grundvermögen, 
			II. Geschäftsstellenausstattung, 
		2. Finanzanlagen:
			I. Beteiligungen an Unternehmen,
			II. sonstige Finanzanlagen; 
	2. Umlaufvermögen:
		1. Forderungen an Gliederungen,
		2. Forderungen aus der staatlichen Teilfinanzierung,
		3. Geldbestände,
		4. sonstige Vermögensgegenstände; 
	3. Gesamtbesitzposten (Summe aus A und B);
2. Schuldposten:
	1. Rückstellungen:
		1. Pensionsverpflichtungen,
		2. sonstige Rückstellungen;
	2. Verbindlichkeiten:
		1. Verbindlichkeiten gegenüber Gliederungen,
		2. Rückzahlungsverpflichtungen aus der staatlichen Teilfinanzierung,
		3. Verbindlichkeiten gegenüber Kreditinstituten,
		4. Verbindlichkeiten gegenüber sonstigen Darlehensgebern,
		5. sonstige Verbindlichkeiten;
	3. Gesamte Schuldposten (Summe von A und B);
3. Reinvermögen (positiv oder negativ).

Erläuterungsteil des Berichtes:    Der Vermögensbilanz ist ein Erläuterungsteil hinzuzufügen, der insbesondere folgende Punkte umfassen muss:
1. Auflistung der Beteiligungen nach Absatz 6 Nr. 1 A II 1 sowie deren im Jahresabschluss aufgeführten unmittelbaren und mittelbaren Beteiligungen, jeweils mit Name und Sitz sowie unter Angabe des Anteils und der Höhe des Nominalkapitals; außerdem sind die Höhe des Anteils am Kapital, das Eigenkapital und das Ergebnis des letzten Geschäftsjahres dieser Unternehmen anzugeben, für das ein Jahresabschluss vorliegt. Die im Jahresabschluss dieser Unternehmen aufgeführten Beteiligungen sind mit den Angaben aus dem Jahresabschluss zu übernehmen. Beteiligungen im Sinne dieses Gesetzes sind Anteile gemäß § 271 Abs. 1 des Handelsgesetzbuchs;
2. Benennung der Hauptprodukte von Medienunternehmen, soweit Beteiligungen an diesen bestehen;
3. im Abstand von fünf Jahren eine Bewertung des Haus- und Grundvermögens und der Beteiligungen an Unternehmen nach dem Bewertungsgesetz (Haus- und Grundvermögen nach §§ 145 ff. des Bewertungsgesetzes).

Anzeigepflicht von Vermögensgegenständen:    In der Vermögensbilanz sind Vermögensgegenstände mit einem Anschaffungswert von im Einzelfall mehr als 5.000 Euro (inklusive Umsatzsteuer) aufzuführen. Vermögensgegenstände sind mit den Anschaffungs- und Herstellungskosten vermindert, um planmäßige Abschreibungen anzusetzen. Im Bereich des Haus- und Grundvermögens erfolgen keine planmäßigen Abschreibungen. Gliederungen unterhalb der Landesverbände können Einnahmen und Ausgaben im Jahr des Zu- beziehungsweise Abflusses verbuchen, auch wenn die jeweiligen Forderungen beziehungsweise Verbindlichkeiten bereits im Vorjahr entstanden sind. Die §§ 249 bis 251 des Handelsgesetzbuchs können für die Aufstellung der Rechenschaftsberichte dieser Gliederungen unbeachtet bleiben.

Anzeigepflicht von hohen Zuwendungen:    Im Rechenschaftsbericht sind die Summe der Zuwendungen natürlicher Personen bis zu 3 300 Euro je Person sowie die Summe der Zuwendungen natürlicher Personen, soweit sie den Betrag von 3 300 Euro übersteigen, gesondert auszuweisen.

Zusammenfassung des Berichtes:    Dem Rechenschaftsbericht ist eine Zusammenfassung voranzustellen:
1. Einnahmen der Gesamtpartei und deren Summe,
2. Ausgaben der Gesamtpartei und deren Summe,
3. Überschuss- oder Defizitausweis,
4. Besitzposten der Gesamtpartei und deren Summe,
5. Schuldposten der Gesamtpartei und deren Summe,
6. Reinvermögen der Gesamtpartei (positiv oder negativ),
7. Gesamteinnahmen, Gesamtausgaben, Überschüsse oder Defizite sowie Reinvermögen der drei Gliederungsebenen Bundesverband, Landesverbände und der ihnen nachgeordneten Gebietsverbände.
 
Neben den absoluten Beträgen zu den Nummern 1 und 2 ist der Vomhundertsatz der
Einnahmesumme nach Nummer 1 und der Ausgabensumme nach Nummer 2 auszuweisen. Zum
Vergleich sind die Vorjahresbeträge anzugeben.
 
Sonstige Einnahmen der PdF sind aufzugliedern und zu erläutern. Einnahmen die im Einzelfall die
Summe von 10.000 Euro übersteigen, sind offen zu legen. Erbschaften und Vermächtnisse sind unter
Angabe ihrer Höhe, des Namens und der letzten Anschrift des Erblassers im Rechenschaftsbericht zu
verzeichnen, soweit der Gesamtwert 10.000 Euro übersteigt.

Meldepflicht der Mitglieder:    Die Anzahl der Mitglieder zum 31. Dezember des Rechnungsjahres ist zu verzeichnen.

Erklärungspflicht am Bericht:    Die Partei fügt dem Rechenschaftsbericht falls nötig zusätzliche Erläuterungen hinzu.

Ausnahmen:    Öffentliche Zuschüsse, die den politischen Jugendorganisationen zweckgebunden zugewendet werden, bleiben bei der Ermittlung der absoluten Obergrenze unberücksichtigt. Sie sind im Rechenschaftsbericht nachrichtlich auszuweisen und bleiben bei der Einnahme- und Ausgaberechnung der Partei unberücksichtigt.

## Spenden
Spendenannahme und Vorgehen:    Die PdF ist berechtigt Spenden anzunehmen. Bis zu einem Betrag von 1 000 Euro kann eine Spende mittels Bargeldes erfolgen. Parteimitglieder, die Empfänger von Spenden an die Partei sind, haben diese unverzüglich an ein für Finanzangelegenheiten von der Partei satzungsmäßig bestimmtes Vorstandsmitglied weiterzuleiten. Spenden sind von einer Partei erlangt, wenn sie in den Verfügungsbereich eines für die Finanzangelegenheiten zuständigen Vorstandsmitglieds oder eines hauptamtlichen Mitarbeiters der Partei gelangt sind; unverzüglich nach ihrem Eingang an den Spender zurückgeleitete Spenden gelten als nicht von der Partei erlangt.

Ausgeschlossene Spenden:    Folgende Spenden sind von der Befugnis der PdF Spenden anzunehmen ausgeschlossen:
1. Spenden von öffentlich-rechtlichen Körperschaften, Parlamentsfraktionen und -gruppen sowie von Fraktionen und Gruppen von kommunalen Vertretungen;
2. Spenden von politischen Stiftungen, Körperschaften, Personenvereinigungen und Vermögensmassen, die nach der Satzung, dem Stiftungsgeschäft oder der sonstigen Verfassung und nach der tatsächlichen Geschäftsführung ausschließlich und unmittelbar gemeinnützigen, mildtätigen oder kirchlichen Zwecken dienen;
3. Spenden von außerhalb des Geltungsbereiches dieses Gesetzes, es sei denn, dass
	1. diese Spenden aus dem Vermögen eines Deutschen im Sinne des Grundgesetzes, eines Bürgers der Europäischen Union oder eines Wirtschaftsunternehmens, dessen Anteile sich zu mehr als 50 vom Hundert im Eigentum von Deutschen im Sinne des Grundgesetzes oder eines Bürgers der Europäischen Union befinden oder dessen Hauptsitz in einem Mitgliedstaat der Europäischen Union ist, unmittelbar der PdF zufließen,
	2. es sich um Spenden an Parteien nationaler Minderheiten in ihrer angestammten Heimat handelt, die diesen aus Staaten zugewendet werden, die an die Bundesrepublik Deutschland angrenzen und in denen Angehörige ihrer Volkszugehörigkeit leben oder
	3. es sich um eine Spende eines Ausländers von nicht mehr als 1 000 Euro handelt;
4. Spenden von Berufsverbänden, die diesen mit der Maßgabe zugewandt wurden, sie an die PdF weiterzuleiten
5. Spenden von Unternehmen, die ganz oder teilweise im Eigentum der öffentlichen Hand stehen oder die von ihr verwaltet oder betrieben werden, sofern die direkte Beteiligung der öffentlichen Hand 25 vom Hundert übersteigt;
6. Spenden, soweit sie im Einzelfall mehr als 500 Euro betragen und deren Spender nicht feststellbar sind, oder bei denen es sich erkennbar um die Weiterleitung einer Spende eines nicht genannten Dritten handelt;
7. Spenden, die der PdF erkennbar in Erwartung oder als Gegenleistung eines bestimmten wirtschaftlichen oder politischen Vorteils gewährt werden;
8. Spenden, die von einem Dritten gegen ein von der PdF zu zahlendem Entgelt eingeworben werden, das 25 vom Hundert des Wertes der eingeworbenen Spende übersteigt.

Spendemeldegrenze:    Spenden, Mitgliedsbeiträge und Mandatsträgerbeiträge an die PdF oder einen oder mehrere ihrer Gebietsverbände, deren Gesamtwert in einem Kalenderjahr (Rechnungsjahr) 10 000 Euro übersteigt, sind unter Angabe des Namens und der Anschrift des Zuwenders sowie der Gesamthöhe der Zuwendung im Rechenschaftsbericht zu verzeichnen. Spenden, die im Einzelfall die Höhe von 50 000 Euro übersteigen, sind dem Präsidenten des Deutschen Bundestages unverzüglich anzuzeigen.

Unzulässige Spenden:     Nach Absatz 13 unzulässige Spenden sind von der Partei unverzüglich, spätestens mit Einreichung des Rechenschaftsberichts für das betreffende Jahr an den Präsidenten des Deutschen Bundestages weiterzuleiten.

# Schlussbestimmungen
Mitglieder als die treibende Kraft:    Die Satzung soll für geregeltes, solidarisches und gemeinschaftliches Leben und Handeln in der Partei sorgen, in der jedes Mitglied, unabhängig von seinen fachlichen Kenntnissen oder persönlichen zeitlichen Einschränkungen, eine möglichst umfassende Beteiligungsmöglichkeit eingeräumt wird. Nur durch die Kooperation und Empathie gelingt es uns Menschen, Organisationen zu schaffen, die die Gesellschaft zum Guten verändern.Diese grundsätzliche Errungenschaft der Menschheit zu beherzigen soll den Antrieb geben für eine basisorientierte und schlagkräftige Partei. Dementsprechend wird die tägliche Arbeit der Unabhängigen Partei geprägt sein von dem Drang sich über den Zustand der Welt zu bilden, von der aufklärerischen Kommunikation auf allen Ebenen, von dem optimalen Transport politischer Inhalte und letztendlich auch von der Bekämpfung von Demokratiedefiziten, Ungerechtigkeit, Vernachlässigungen und Armut in Deutschland und der Welt.

Salvatorische Klausel:    Sollte eine der Bestimmungen in dieser Satzung ganz oder teilweise rechtswidrig oder unwirksam sein oder werden, so wird die Gültigkeit der übrigen Bestimmungen dadurch nicht berührt; in einem solchen Fall ist die Satzung vielmehr ihrem Sinne gemäß zur Durchführung zu bringen. Anwendung des gesetzlichen Maßes bei ungültigen Leistungs- und Zeitbestimmungen: Beruht die Ungültigkeit auf einer Leistungs- oder Zeitbestimmung, so tritt an ihre Stelle das gesetzlich zulässige Maß. Änderung der Satzung im Falle einer rechtswidrigen oder unwirksamen Klausel: Die rechtswidrige oder unwirksame Bestimmung ist unverzüglich durch Beschluss der Mitgliederversammlung zu ersetzen oder zu entfernen.